package com.dxc.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dxc.entity.Post;

public interface PostRespository extends JpaRepository<Post, Long>{
	
}
