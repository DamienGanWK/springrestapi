package com.dxc.Service;

import java.util.List;
import java.util.stream.Collectors;

import com.dxc.Payload.PostDTO;
import com.dxc.Payload.PostResponse;
import com.dxc.Respository.PostRespository;
import com.dxc.Service.Impl.PostServiceInterface;
import com.dxc.entity.Post;
import com.dxc.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImplementation implements PostServiceInterface {

	@Autowired
	private PostRespository postRespository;

	private PostDTO mapToDto(Post post) {
		PostDTO postDto = new PostDTO();
		postDto.setId(post.getId());
		postDto.setTitle(post.getTitle());
		postDto.setDescription(post.getDescription());
		postDto.setContent(post.getContent());

		return postDto;
	}

	public Post mapToEntity(PostDTO postDto) {
		Post post = new Post();
		post.setId(null);
		post.setTitle(postDto.getTitle());
		post.setDescription(postDto.getDescription());
		post.setContent(postDto.getContent());
		return post;

	}

	// implementing Creat Postblog
	public PostDTO createPost(PostDTO postDto) {
		Post post = mapToEntity(postDto);
		Post newPost = postRespository.save(post);

		// convert entty to DTO
		PostDTO postResponse = mapToDto(newPost);
		return postResponse;
	}

	// implementing the Get PostBlog
	@Override
	public PostResponse getAppPosts(int pageNo, int pageSize, String sortBy, String sortDir) {
		Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();

		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		Page<Post> posts = postRespository.findAll(pageable);

		// get content from page object
		List<Post> listOfPosts = posts.getContent();

		List<PostDTO> content=listOfPosts.stream().map(
				post -> mapToDto(post)).collect(Collectors.toList()
				);
		
		PostResponse postResponse = new PostResponse();
		postResponse.setContent(content);
		postResponse.setPageNo(posts.getNumber());
		postResponse.setPageSize(posts.getSize());
		postResponse.setTotalElement(posts.getTotalElements());
		postResponse.setTotalPages(posts.getTotalPages());
		postResponse.setLast(posts.isLast());
		return postResponse;
	}

	@Override
	public PostDTO getPostById(Long id) {
		Post post = postRespository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id));
		return mapToDto(post);
	}

	@Override
	public PostDTO updatePost(PostDTO postDto, Long id) {
		Post post = postRespository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id));
		post.setTitle(postDto.getTitle());
		post.setDescription(postDto.getDescription());
		post.setContent(postDto.getContent());
		
		Post updatePost = postRespository.save(post);
		return mapToDto(updatePost);	
	}

	@Override
	public void deletePostById(Long id) {
		Post post = postRespository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id));
		postRespository.delete(post);
	}

}
